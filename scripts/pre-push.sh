#!/usr/bin/env bash
set -e
DIR=$(dirname "$0")
cd ${DIR}/../..

if ! $(git diff HEAD --quiet); then
    echo 'error: cannot run pre-push hook: There are uncommitted changes.'
    echo 'To preserve changes:'
    echo '    Run "git stash"'
    echo '    Run "git push" again'
    echo '    Run "git stash pop"'
    echo ''
    exit 1
fi

echo "Running flake8"
pipenv run flake8 --config=scripts/.flake8
echo "flake8 OK :)"

echo "Running pylint"
pipenv run pylint --rcfile=scripts/.pylintrc project
echo "pylint OK :)"

echo "Running py.test"
pipenv run pytest -c scripts/pytest.ini --cov --cov-config scripts/.coveragerc --junitxml=../test-results/xunit-result-master.xml;
echo "py.test OK :)"

echo "Running eslint"
npx eslint --config scripts/.eslintrc.js "project/**/*.js"
echo "eslint OK :)"

echo "Running jscpd"
npm run jscpd --config scripts/.cpd.yaml
echo "jscpd OK :)"

